# Simple Quest Personal Documentation


[Ccылка](https://www.unrealengine.com/marketplace/en-US/product/easy-quests) на ассеты, в контексте которых пишется данная документация

# Структуры

**ST_Quest_Info** - основная структура, содержит всю информацию о квесте. На основе этой структуры была создана таблица DT_Quests, из которой в свою очередь и берется вся информация для квестов.

| Название | Тип данных | Значение по умолчанию |
| ------ | ------ | ------ |
| QuestID | FName | None |
| QuestName | FText |  |
| Category | E_QuestCategory | MainQuest |
| AreaName | E_Areas | Georgedens |
| RecommendedLevel | Integer | 0 |
| Description | FText |  |
| Objectives | Массив ST_Quest_Objectives |  |
| Rewards | ST_Quest_Rewards | ST_Quest_Rewards по умолчанию |
| RequiredQuests | Массив FName |  |
| QuestsToAddAfterCompletion | Массив FName |  |
| CanQuestBeAborted? | Boolean | false |

**ST_Quest_Objectives** - структура, содержащая информацию о цели(их может быть несколько) квеста.

| Название | Тип данных | Значение по умолчанию |
| ------ | ------ | ------ |
| Objective_ID | FName | None |
| ObjectiveDescription | FText |  |
| ObjectiveTips | Массив FText |  |
| CurrentAmount | Integer | 0 |
| RequiredAmount | Integer | 0 |
| HasWorldMarker? | Boolean | false |
| ObjectiveCompleteAnotherQuest? | Boolean | false |
| QuestID | FName | None |

**ST_Quests_Rewards** - структура, содержащая информацию о наградах за выполнение квеста.

| Название | Тип данных | Значение по умолчанию |
| ------ | ------ | ------ |
| Experience | Float | 0,0 |
| Gold | Integer | 0 |
| Items | Map элементы, хранящие FName и Integer |  |

## Энумерации
**E_Areas** - названия локаций, в которых был взят квест. Можно будет поменять на свои.
| Локация |
| ------ |
| None |
| Georgedens |
| Hokil |
| Cape Wardeast |
| Monhad |
| Port Oakden |

**E_QuestCategory** - квестовые категории.
| Категория |
| ------ |
| MainQuest |
| SideQuest |

**E_QuestNotification** - уведомления квестов.
| Тип |
| ------ | 
| OneQuest |
| MultipleQuests |
| QuestCompleted |
| QuestAborted |

**Filter_Type**
| Тип |
| ------ |
| None |
| Main |
| Side |
| Completed |
| Aborted |

# Блюпринт интерфейсы
### BP_BPI_Quests
**Функции**
| Название | Принимает | Возвращает |
| ------ | ------ | ------ |
| GetGold |  | TotalGold(Integer) |
| GetExperience |  | Experience(Float) |
| GetLevel |  | Level(Integer) |
| GetNeededExperience |  | NeededExperience(Float) |
| ModifyGold | Amount(Integer) |  |
| ModifyExperience | Amount(Float) |  |
| LoadExpGoldInfo | <p>Level(Integer),<br>Gold(Integer),<br>Experience(Float)<br>NeededExperience(Float)</p> |  |
| ToggleMouseInput |  |  |

### BP_BPI_QuestsInteraction
**Функции**
| Название | Возвращает |
| ------ | ------ |
| PlayerActor | AActor |

# Блюпринт функциональные библиотеки
### BP_BPL_Quests
**Pure функции**
| Название | Принимает | Возвращает | Описание |
| ------ | ------ | ------ | ------ |
| [GetQuestComponent](https://blueprintue.com/blueprint/_f6557jr/) |  | <p>ComponentWasFound?(Boolean)<br>QuestComponent(BP_BPC_PlayerQuestComponent)</p> | Ищет у игрока с индексом 0 BP_BPC_PlayerQuestComponent. Возвращает true и ссылку на компонент, если он валиден.|
| [IsValidQuest?](https://blueprintue.com/blueprint/slb7y6an/) | ST_Quest_Info | Valid?(Boolean) | <p>Возвращает true, если<p>ST_Quest_Info->QuestID != 0,<br>ST_Quest_Info->QuestName != None и<br>ST_Quest_Info->Objectives.Length() > 0.</p> |

# Блюпринт классы

## BP_BPC_PlayerQuestComponent

**PlayerQuestComponent** – основной компонент ассетов EesyQuest, добавляющий персонажу функционал заданий.

### Хранит переменные:

| Название | Тип данных | Значение по умолчанию | Пояснения |
| ------ | ------ | ------ | ------ |
| QuestsObjectives | Массив ST_Quest_Objectives |  |  |
| ActiveQuests | Массив ST_Quest_Info |  |  |
| FinishedQuests | Массив FName |  |  |
| QuestWidget | WBP_QuestWidget |  | Основной класс интерфейса квест лога |
| StartingQuests | Массив FName |  |  |
| AbortedQuests | Массив FName |  |  |
| MaximumActiveQuests | Integer | 10 |  |
| SaveSlotString | String | “EasyQuestsSave” |  |
| QuestsSaveObject | BP_QuestsSaveObject |  |  |

### Делегаты:

| Название | Принимает данные |
| ------ | ------ |
| **OnQuestAdded** | QuestID (FName), QuestInfo(структура ST_Quest_Info) |
| **OnObjectiveUpdated** | ObjectiveID (FName), Objective (структура ST_Quest_Objectives), QuestInfo (структура ST_Quest_Info) |
| **OnQuestCompleted** | QuestID (FName), Quest (структура ST_Quest_Info) |
| **OnQuestAborted** | QuestID (FName), QuestInfo (структура ST_Quest_Info) |
| **OnTrackedQuestChanged** | QuestID (FName), Quest (структура ST_Quest_Info) |
| **OnQuestsLoaded** |  |

### EventGraph:

| Название | Принимает | Описание |
| ------ | ------ | ------ |
| [Event Begin Play](https://blueprintue.com/blueprint/s9vr4mnf/) |  | <p>Создает виджет **WBP_QuestWidget**; <br>**LoadQuest**; <br>Бинды эвентов по делегатам **OnQuestCompleted**, **OnQuestAdded**, **OnObjectiveUpdated**, **OnQuestAborted**; <br>**AssignStartingQuests**.</p> |
| [OnQuestCompleted_Event](https://blueprintue.com/blueprint/kxjzy1n6/) | <p>QuestID(FName)<br>Quest(ST_Quest_Info)</p> | Отображает соответствующее уведомление и проверяет наличие каких-либо целей “Завершение квеста”, которые сейчас могут быть выполнены. |
| [OnQuestAdded_Event](https://blueprintue.com/blueprint/fhqm8mcx/) | <p>QuestID(FName)<br>QuestInfo(ST_Quest_Info)<br>MultipleQuestsAdded?(Boolean)</p> | Отображает соответствующее уведомление квеста в виджете после Delay. **(Важно: нежеалательно использовать Delay в коде!)** |
| [OnTrackedQuestChangedThroughObjective_Event](https://blueprintue.com/blueprint/ey56bbn_/) | <p>ObjectiveID(FName)<br>Objective(ST_Quest_Objective)<br>QuestInfo(ST_Quest_Info)</p> | Вызов делегата **OnTrackedQuestChanged**, не забинженного в **Event Begin Play**. |
| [OnQuestAborted_Event](https://blueprintue.com/blueprint/c9jio7uo/) | <p>QuestID(FName)<br>QuestInfo(ST_Quest_Info)</p> | Отображает соответствующее уведомление квеста в виджете и также обновляет в квест логе. **(Важно: нежеалательно использовать Delay в коде!)** |

### Функции:

| Название | Принимает | Возвращает | Описание |
| ------ | ------ | ------ | ------ |
| [AddQuest](https://blueprintue.com/blueprint/lmkuw842/) | <p>QuestID(FName)<br>CallDelegate?(Boolean)</p> | Added?(Boolean) | Проверяет переменную QuestID функцией CanQuestBeAccepted?. Added? возвращает false, если проверка прошла безуспешно. При успешной проверке, соответствующий QuestID используется для занесения новый целей(GetQuestObjectives) в QuestsObjectives и нового задания(GetQuestInfo) в ActiveQuests. После чего вызывается функция SaveQuests. Если CallDelegate? == true, то вызывается делегат OnQuestAdded(QuestID, GetQuestInfo(QuestID), false). |
| [AddObjectiveAmount](https://blueprintue.com/blueprint/vbkrdd_e/) | <p>QuestID(FName)<br>ObjectiveID(FName)<br>AmountToAdd(Integer)</p> |  | Добавляет значение AmmountToAdd к текущему числу раз выполнения цели CurrentAmount. Необхимый квест ищется по QuestID(FindQuestByID), а цели по ObjectiveID(FindObjectiveByID). Данные по каждой найденной цели обновляются в QuestsObjective и ActiveQuests, вызывается делегат OnObjectiveUpdated, функции SaveQuests, провека CheckIfQuestCanBeCompleted(если успешно, то запускается CompleteQuest). |
| [CheckIfQuestCanBeCompleted](https://blueprintue.com/blueprint/j_aa1x30/) | QuestID(FName) | Completed?(Boolean) | Ищет активные цели квеста по QuestID(GetActiveQuestObjectives). Если они успешно найдены, то происходит проверка их элементов на предмет CurrentAmount >= RequiredAmount. Если в ходе провеки хотя бы одна цель противоречит этому, то Completed? возвращает false.  |
| [CompleteQuest](https://blueprintue.com/blueprint/u9ytyrq1/) | Quest(ST_Quest_Info) |  | Из массива ActiveQuests вынимается полученная ST_Quest_Info и его QuestID заносится в FinishedQuests. Вызывается делегат OnQuestCompleted. Используется функция StartMultipleQuests(по сути эти квесты - награда за выполнение текущего). В игроке с индексом 0 вызывается ModifyExperience и ModifyGold(функции интерфейса BP_BPI_Quests, должны быть перезагружены в классе персонажа, которому принадлежит компонент). Заканчивается функция вызовом SaveQuests. В комментарии создатель сообщает "Не забудьте вызвать делегат и запустить **Quests to Add After Completion**". Так же подмечается, что "Возможно, вы захотите соединить вознаграждения с вашей собственной системой, если она у вас есть". Определенно, стоит иметь это ввиду если проект подразумевает продвинутый инвентарь и статы персонажа. |
| [Toggle Quest Log Visibility](https://blueprintue.com/blueprint/qphqlrr_/) |  |  | Выставляет Visibility(Visible или Collapsed) для QuestWidget->WBP_Quest_Log. Если текущее Visibility == Visible, выставляет Collapsed, в противном случае - Visible. После во владельце компонента вызывается функция ToggleMouseInput(интерфейс BP_BPI_Quests). |
| [AssignStartingQuests](https://blueprintue.com/blueprint/bjk9khjb/) |  |  | Вызывает функцию StartMultipleQuests с входными данными в виде массива StartingQuests. |
| [StartMultipleQuests](https://blueprintue.com/blueprint/rq24095c/) | QuestIDs(массив FName) |  | Пробует добавить каждый квест, полученный из массива QuestIDs(AddQuest без вызова делегата). По окончанию добавления, если квестов больше одного, вызывает OnQuestAdded, в котором MultipleQuestsAdded = true без входных данных QuestID и QuestInfo. Если квест всего один, то OnQuestAdded вызывается с параметрами соотетствующие единственному найденному квесту и MultipleQuestsAdded = false. |
| [AbortQuest](https://blueprintue.com/blueprint/ryi86q94/) | Quest(ST_Quest_Info) |  | Прерывает квест, извлекает отслеживаемый виджет, вызывает делегату OnQuestAborted, заносит QuestID в AbortedQuests, извлекает квест из ActiveQuests, обновляет квест лог(PopulateQuestsList(false, false), ResetInformation), выполняет SaveQuests. |
| [TrackQuest](https://blueprintue.com/blueprint/a0rfl148/) | Quest_Info(ST_Quest_Info) |  | Отслеживает квест. Вызывает OnButtonClicked, если функция FindQuestButtonWithQuest прошла успешно. |
| [SaveQuests](https://blueprintue.com/blueprint/7chnhdv1/) |  |  | Если QuestsSaveObject не валиден, создает его и заносит новый файл сохранения BP_QuestsSaveObject. Далее заносит в файл всю актуальную информацию, а именно: AbortedQuests, ActiveQuests, FinishedQuests, QuestsObjectives, FilterType и CurrentQuest(из QuestWidget), данные персонажа с индексом 0(Gold, Level, Experience, NeededExperience). Сохраняет файл в слот с именем, указанном в SaveSlotString по UserIndex 0. |
| [LoadQuests](https://blueprintue.com/blueprint/xlhgkq_7/) |  |  | Загружает игру по слоту, написанному в SaveSlotString по юзер индексу 0 с кастом на BP_QuestsSaveObject. Если каст прошёл успешно, данный класс заносится в QuestsSaveObject, а далее выставляются все данные(аналогичны сохраненным) и вызывается делегат OnQuestsLoaded. |

### Pure функции:

| Название | Принимает | Возвращает | Описание |
| ------ | ------ | ------ | ------ |
| [CanQuestBeAccepted?](https://blueprintue.com/blueprint/2wbqdjr4/) | QuestID(FName) | Out(Boolean) |<p>Возвращает true, если <p>IsQuestCompleted?(QuestID) == false, <br>HasFinishedQuests?(GetQuestInfo(QuestID)->RequiredQuests) == true, <br>IsQuestActive?(QuestID)->Out == false, <br>WasQuestAborted?(QuestID) == false, <br>IsValidQuest?(QuestID) == true и<br>ActiveQuests.Lenght() <= MaximumActiveQuests.</p>|
| [GetQuestObjectives](https://blueprintue.com/blueprint/_x1djnmq/) | QuestID(FName) | Objectives(Массив ST_Quest_Objectives) | Ищет в таблице DT_Quests строку, эквивалентную переменной QuestID. При успешном нахождении строки возвращает массив Objectives из таблицы, в противном случае возвращает пустой массив. Создатель рекомендует использовать в основном для значений по умолчанию. |
| [GetQuestInfo](https://blueprintue.com/blueprint/2qlsga6j/) | QuestID(FName) | Quest(ST_Quest_Info) | Ищет в таблице DT_Quests строку, эквивалентную переменной QuestID. При успешном нахождении строки, возвращает структуру ST_Quest_Info. Создатель рекомендует использовать в основном для значений по умолчанию. |
| [Find Objectives by ID](https://blueprintue.com/blueprint/fawefo2z/) | ObjectiveID(FName) | <p>Found(Boolean)<br>Objectives(Массив ST_Quest_Objectives)<br>FoundAt(Массив Integer)</p> | Проверяет массив QuestsObjectives на предмет наличия целей, эквивалентных ObjectID. Возвращает индексы и ST_Quest_Objectives каждой успешной проверки. Если хотя бы один раз проверка прошла успешно, то в Found возвращается true. |
| [HasFinishedQuests?](https://blueprintue.com/blueprint/x-nz0ms2/) | QuestsID(Массив FName) | Finished?(Boolean) | Возвращает true, если все элементы полученного массива находятся в массиве FinishedQuests. Создатель подписал в комментарии, что данная функция используется для "включенных квестов." |
| [IsQuestActive?](https://blueprintue.com/blueprint/4_xpqm1o/) | QuestID(FName) | <p>Out(Boolean)<br>Quest(ST_Quest_Info)</p> | Возвращает true, если квест находится в массиве ActiveQuests и структуру ST_Quest_Info этого квеста. |
| [IsQuestCompleted?](https://blueprintue.com/blueprint/f7g_h4er/) | QuestID(FName) | Out(Boolean) | Возвращает true, если QuestID находится в массиве FinishedQuests. |
| [FindQuestByID](https://blueprintue.com/blueprint/wnvjyvei/) | QuestID(FName) | <p>Found(Boolean)<br>Quest(ST_Quest_Info)<br>FoundAt(Integer)</p> | Ищет в массиве ActiveQuest по QuestID. Возвращает ST_Quest_Info и индекс массива. Если квест не найден, Found возвращает false. |
| [GetActiveQuestObjectives](https://blueprintue.com/blueprint/892q6a6t/) | QuestID(FName) | <p>Found(Boolean)<br>Objectives(SQ_Quest_Objectives)<br>FoundAtObjectives(Integer)<br>FoundAtQuests(Integer)</p> | <p>Находит текущие цели задания.<p>Found возвращает true, если хотя бы одна активная цель найдена в соответствующем QuestID задании;<br>Objectives возвращает найденные ST_Quest_Objective;<br>FoundAtObjectives возвращает индексы, в которых была найдена цель;<br>FoundAtQuests возвращает индекс квеста, в котором были найдены цели.<p>Создатель подмечает, что функцию стоит использовать для текущих значений, а не для значений по умолчанию</p> |
| [GetCurrentObjectiveForActiveQuest](https://blueprintue.com/blueprint/jty86s1o/) | QuestID(FName) | <p>FoundObjective?(Boolean)<br>FoundObjectiveInfo(ST_Quest_Objectives)</p> | Возвращает текущее отображаемую цель активного квеста. ST_Quest_Info находится по функции FindQuestByID. Если нахождение прошло успешно, то начинается проверка целей. Первый элемент, в котором CurrentAmmount < RequiredAmount, возвращается в FoundObjectiveInfo, а FoundObjective? возвращает true. В иных случаях FoundObjective? возвращает false. |
| [WasQuestAborted?](https://blueprintue.com/blueprint/72km0qtq/) | QuestID(FName) | Out(Boolean) | Возврашает true, если QuestID содержится в AbortedQuests. |
| [IsValidQuest?](https://blueprintue.com/blueprint/nw13up3p/) | QuestID(FName) | Valid?(Boolean) | <p>Не считая того, что ST_Quest_Info находится по внутренней pure-функции GetQuestInfo, аналогично IsValidQuest? из интерфейса BP_BPL_Quests.<p>Возвращает true, если<p>ST_Quest_Info->QuestID != 0,<br>ST_Quest_Info->QuestName != None и<br>ST_Quest_Info->Objectives.Length() > 0.</p>  |

## BP_Quest_Character

**Quest_Character** является аналогичным тому персонажу, который создается в стартер паке для проекта от 3 лица. Помимо этого, присутствуют новые переменные, BPC_PlayerQuestComponent, интерфейс BP_BPI_Quests. В EventGraph содержится QuestNode. Автор подписал скопировать данную ноду для своего собственного персонажа, после добавления "Quests" интерфейса). 

### Новые переменные:

| Название | Тип данных | Значение по умолчанию |
| ------ | ------ | ------ |
| InteractionRange | Integer | 150 |
| MouseVisibility | Boolean | False |
| Gold | Integer | 0 |
| Experience | Float | 0,0 |
| NeededExperience | Float | 50,0 |
| Level | Integer | 1 |

### QuestNode:

| Название | Принимает | Возвращает | Описание |
| ------ | ------ | ------ | ------ |
| Event Modify Experience(BP_BPI_Quests) | | Amount(Float) | Вызывает кастомный эвент AddExperience. |
| Event Modify Gold(BP_BPI_Quests) | | Amount(Integer) | Вызывает кастомный эвент AddGold. |
| AddGold | | Amount(Integer) | Изменяет значение Gold(Gold + Amount). |
| AddExperience | | Amount(Float) | Изменяет значение Experience(Experience + Amount). Далее, если новое значение Experience >= NeededExperience, выставляет новые значения для Level(Level + 1) и NeededExperience(NeededExperience * 1,5), пока Experience вновь не станет меньше NeededExperience. |
| SaveEXPGold | | | Вызывает в BPC_PlayerQuestComponent SaveQuests. В данный момент этот эвент не используется, но сделана на случай если нужно будет сохраняться вручную. |
| Event Load Exp Gold Info(BP_BPI_Quests) | | <p>Level(Integer)<br>Gold(Integer)<br>Experience(Float)<br>NeededExperience(Float)</p> | Выставляет загруженные значения для Level, Gold, Experience, NeededExperience. |
| Input K_Pressed | | | Вызывает в BPC_PlayerQuestComponent ToogleQuestLogVisibility. |
| Input E_Pressed | | | Справнит трейс SphereTraceForObject длиной, указанной в InteractionRange, по форвард вектору. Игнорирует себя и реагирует на WorldDynamic и Pawn. При успешном нахождение соответствующего объекта, вызывает в нем OnInteract(BP_BPI_Quest_Interaction), указывая ссылку на себя.  |
| Event Toogle Mouse Input | | | Вызывает функцию Mouse Input. |
| GetNeededExperience(BP_BPI_Quests) | NeededExperience(Float) | | |
| GetLevel(BP_BPI_Quests) | Level(Integer) | | |
| GetExperience(BP_BPI_Quests) | Experience(Float) | | |
| GetGold(BP_BPI_Quests) | TotalGold(Integer) | | |

### Функции:

| Название | Описание |
| ------ | ------ |
| MouseInput | Меняет отображение курсора в PlayerController по индексу 0 в зависимости от того, отображается сейчас курсор или нет. |

## BP_In_Collectables

**In_Collectables** является объектом квестов на собирательство, хранит WorldMarker(WidgetComponent), активирующийся при отслеживании относящейся цели(ObjectiveID), относящегося квеста(QuestID).

### Хранит переменные

| Название | Тип данных | Значение по умолчанию | Пояснение |
| ------ | ------ | ------ | ------ |
| QuestComponent | BP_BPC_PlayerQuestComponent | | |
| QuestID | FName | None | Указывается в редакторе |
| ObjectiveID | FName | None | Указывается в редакторе |
| InteractablesSaveObject | BP_InteractablesSaveObject | | |
| InteractablesSaveString | String | InteractablesSave | |

### EventGraph: 

| Название | Принимает | Описание |
| ------ | ------ | ------ |
| [Event Begin Play](https://blueprintue.com/blueprint/suek6bcw/) | | Указывает имя объекта в InteractablesSaveString и проверяет существует ли сейв-файл с таким именем. Если есть, то заносит его в InteractablesSaveObject, в противном случае создает новый сейв-файл. Проверяет в нем переменную HasBeenInteractedWith. Если HasBeenInteractedWith == true, актор удаляется, в противном случае использует GetQuestComponent(BP_BPL_Quests). Если компонент не найден, на экране отображается сообщение "Quest Component was not found.", в другом случае заносится в QuestComponent. Биндит эвент OnTrackedQuest на функцию UpdateWorldMarker. Запускает функцию UpdateWorldMarker без входных данных. Заносит ссылку на себя в виджет WorldMarker.  |
| [Event OnInteract(BP_BPI_QuestsInteraction)](https://blueprintue.com/blueprint/vr740lit/) | PlayerActor(AActor) | Проверяет активен ли квест в данный момент(IsQuestActive?). Если Out == true, то запускает функцию AddObjectiveAmount(QuestID, ObjectiveID, 1). Устанавливает в InteractableSaveObject HasBeenInteractedWith в true, сохраняет эту информацию и актор удаляется. |

### Функции:

| Название | Принимает | Описание |
| ------ | ------ | ------ |
| UpdateWorldMarker | <p>QuestID(FName)<br>Quest(ST_Quest_Info)</p> | <p>Выставляет Visibility WorldMarker в false. Если Quest->QuestID == QuestID, то начинается проверка всех целей квеста.<p> Visibility WorldMarker станет true, если все эти условия будут выполнены:<p>ST_Quest_Objectives->ObjectiveID == Objective;<br>ST_Quest_Objectives->HasWorldObjectives? == true;<br>Выходные данные QuestComponent->GetCurrentObjectiveForActiveQuest(QuestID)  FoundObjective == true и<br>FoundObjectiveInfo->ObjectiveID == ObjectiveID;<br>QuestComponent->WasQuestAborted?(QuestID) == false.</p>|

## BP_In_Enemy

**In_Enemy** - класс противника. Все используемые функции, эвенты и переменные аналогичны BP_In_Collectables. 

## BP_In_Location

**In_Location** - область, в которую нужно войти чтобы выполнить задание. Все используемые функции, эвенты и переменные аналогичны BP_In_Collectables, кроме того, что логика **Event OnInteract(BP_BPI_QuestsInteraction)** выполняется теперь в том случае, если что-то вошло в компонент коллизии.

## BP_In_NPC

**In_NPC** - класс NPC, дающего задания. Присутствует компонент TextRander для "!" и WorldMarker.

### Хранит переменные:

| Название | Тип данных | Значение по умолчанию | Пояснение |
| ------ | ------ | ------ | ------ |
| QuestComponent | BP_BPC_PlayerQuestComponent | | |
| QuestsToAssign | Массив FName | | Указывается в редакторе |
| Quest IDs | Массив FName | | Указывается в редакторе |
| Objective IDs | Массив FName | | Указывается в редакторе |

### EventGraph: 

| Название | Принимает | Описание |
| ------ | ------ | ------ |
| [BP_In_NPC (Event BeginPlay)](https://blueprintue.com/blueprint/r90q11nj/) | | Используется функция GetQuestComponent. Если компонент не найден, то на экране отображается сообщение "Quest Component was not found". В другом случае, он заносится в QuestComponent. В нем биндится функция UpdateWorldMarker по делегату OnTrackedQuestChanged. Вызывается эта функция без выходных данных. Используется Delay с длительностью 0,0(**Не рекомендуется использовать в коде!**). В том же QuestComponent биндится фунуция UpdateQuestMark по делегату OnQuestCompleted. Вызывается эта функция без выходных данных. В WorldMarker заносится ссылка на себя. |
| [Event OnInteract(BP_BPI_QuestsInteraction)](https://blueprintue.com/blueprint/dirq04gi/) | PlayerActor(AActor) | Запускается функция CheckForAnyObjectives. Если поиск целей оказался безуспешным, то запускается в QuestComponent запускается функция StartMultipleQuests по данным массива QuestsToAssign. UpdateQuestMark без входных данных. UpdateWorldMarker без входных данных. |

### Функции:

| Название | Принимает | Возвращает | Описание |
| ------ | ------ | ------ | ------ |
| [UpdateQuestMark](https://blueprintue.com/blueprint/uqkitl4i/) | <p>QuestID(FName)<br>Quest(ST_Quest_Info)</p> | | Выставляет Visibility для TextRender false. Проверяет QuestComponent на валидность, а дальше проверяет элементы QuestsToAssign функцией CanQuestBeAccepted? в QuestComponent. Если хотя бы один элемент в ходе проверки выведет true, Visibility TextRender будет выставлен в true.|
| [UpdateWorldMarker](https://blueprintue.com/blueprint/rhgrzzv5/) | <p>QuestID(FName)<br>Quest(ST_Quest_Info)</p> | | Не считая того, что идет сравнение с несколькими квестами, занесенными в массив, а не c одним, аналогично функции с таким же названием в других классах. |
| [Check For Any Objectives](https://blueprintue.com/blueprint/k0vth4pv/) | | Out(Boolean) | Проверяет цели активных квестов(GetCurrentObjectiveForActiveQuest) занесенных в Quest IDs на наличие тех, которые соответствуют по названию занесенным в ObjectiveIDs. Для каждой соответствующей цели вызывает AddObjectiveAmount. Возвращает в Out true, если хотя бы одна такая цель была успешно выполнена.|

## BP_InteractablesSaveObject

**InteractablesSaveObject** хранит сохраненную информацию о итерации в находящемся объекте.

### Хранит переменные:

| Название | Типа данных | Значение по умолчанию |
| ------ | ------ | ------ |
| HasBeenInteractedWith | Boolean | False |

## BP_QuestsSaveObject

**QuestsSaveObject** хранит сохраненную информацию, необходимую PlayerQuestComponent.

### Хранит переменные:

| Название | Типа данных | Значение по умолчанию |
| ------ | ------ | ------ |
| SavedActiveQuests | Массив ST_Quest_Info | |
| SavedFinishedQuests | Массив FName | |
| SavedAbortedQuests | Массив FName | |
| SavedQuestsObjectives | Массив ST_Quest_Objectives | |
| SavedCurrentQuest | ST_Quest_Info | |
| SavedFilterType | FilterType | None |
| SavedLevel | Integer | 0 |
| SavedGold | Integer | 0 |
| SavedExperience | Float | 0,0 |
| SavedNeededExperience | Float | 0,0 |

### Функции
| Название | Принимает | Возвращает | Описание |
| ------ | ------ | ------ | ------ |
| SaveExpGold | <p>Level(Integer)<br>Gold(Integer)<br>Experience(Float)<br>NeededExperience(Float)</p> |  | Заносит полученные данные персонаж в соответствующие переменные. |
| LoadExpGold |  | <p>SavedLevel(Integer)<br>SavedGold(Integer)<br>SavedExperience(Float)<br>SavedNeededExperience(Float)</p> | Возвращает соответствующие переменные персонажа. |
